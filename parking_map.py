import numpy as np
import cv2
import networkx as nx
import time
from drawer import *
from constant import *

class ParkingLot:
	def __init__(self, parking_lot, index, is_banned, is_obstacle=False, name=""):
		self.lot = parking_lot
		self.index = index
		self.is_banned = is_banned
		self.is_obstacle = is_obstacle
		self.is_full = False
		self.is_reserved = False
		if (self.index <= 12):
			self.name = "A" + str(index).zfill(2)
		elif (self.index <= 26):
			self.name = "B" + str(index - 13).zfill(2)
		elif (self.index <= 40):
			self.name = "C" + str(index - 27).zfill(2)
		else:
			self.name = "D" + str(index - 41).zfill(2)


class ParkingMap:

	def get_id(self, point):
		return self.moveable_points.index(point)

	def gen_graph_path(self, path):
		G = nx.Graph()
		for i in range(1, len(path)):
			G.add_edge(path[i - 1], path[i])
		return G

	def get_parking_lot_centroid(self, parking_lot_id):
		return get_centroid(self.parking_lots[parking_lot_id].lot)

	def read_parking_lots(self, parking_lots_file):
		parking_lot = []
		with open(parking_lots_file, "r") as file_in:
			while (1):
				line = file_in.readline()
				if (len(line) == 0):
					break
				lot = []
				numbers = line.split(" ")
				size = len(numbers)
				for i in range(1, size - 1, 2):
					lot.append((int(numbers[i]), int(numbers[i + 1])))
				index = int(numbers[0])
				is_banned = int(numbers[size - 1])
				is_obstacle = is_banned
				parking_lot.append(ParkingLot(lot, index, is_banned, is_obstacle))
		return parking_lot

	def read_moveable_points(self, moveable_points_file):
		moveable_points = []
		with open(moveable_points_file, "r") as file_in:
			while (1):
				line = file_in.readline()
				if (len(line) == 0):
					break
				numbers = line.split(" ")
				moveable_points.append((int(numbers[0]), int(numbers[1])))
		return moveable_points


	def is_valid_path(self, p, q):
		for lot_info in self.parking_lots:
			lot = lot_info.lot
			index = lot_info.index
			is_banned = lot_info.is_banned
			if (is_banned == 0):
				continue
			if (is_lot_intersection(p, q, lot)):
				return False
		return True


	def init_graph(self):
		self.graph = nx.Graph()
		for lot_info in self.parking_lots:
			self.moveable_points.append(get_centroid(lot_info.lot))
		for i_p, p in enumerate(self.moveable_points):
			for i_q, q in enumerate(self.moveable_points):
				if (i_p >= i_q):
					continue
				if (euclidean_distance(p, q) > 100):
					continue
				if (self.is_valid_path(p, q)):
					self.graph.add_edge(i_p, i_q, weight=euclidean_distance(p, q))
					# print(p, q)

		time1 = time.time()
		# for edge in self.graph.edges:
		# 	if self.is_lot_intersection(self.moveable_points[0], self.moveable_points[1], self.parking_lots[0][0]):
		# 		pass
		print("graph size : ", len(self.graph.edges))
		time2 = time.time()
		print("time : ", time2 - time1)
		# draw_map(self.image, self.parking_lots, self.moveable_points, self.graph)

	def convert_to_image(self, custom_graph = None):
		if (custom_graph is None):
			return draw_map(self.image, self.parking_lots)
		return draw_map(self.image, self.parking_lots, self.moveable_points, custom_graph)

	def update_graph(self, list_updated_id):
		for edge in self.graph.edges:
			u = edge[0]
			v = edge[1]
			weight = self.graph[u][v]["weight"]
			if (u in list_updated_id) or (v in list_updated_id):
				self.graph[u][v]["weight"] = MAX_WEIGHT

	def remove_temp_vertexs(self):
		for edge in self.graph.edges:
			u = edge[0]
			v = edge[1]
			if "temp" in self.graph[u][v]:
				self.graph.remove_edge(u, v)
		while (len(self.moveable_points) > self.num_points):
			self.moveable_points.pop()

	def add_temp_vertex(self, new_point):
		self.moveable_points.append(tuple(new_point))
		new_vertex = len(self.moveable_points) - 1
		for id in range(self.num_points):
			point = self.moveable_points[id]
			dis = euclidean_distance(point, new_point)
			if (dis <= 100):
				self.graph.add_edge(new_vertex, id, weight=dis, temp=1)

	def update_map(self, tracked_objects):
		self.remove_temp_vertexs()
		list_updated_id = set()
		is_parked = [0] * len(tracked_objects)

		for lot_info in self.parking_lots:
			lot_info.is_full = False

		for lot_info in self.parking_lots:
			lot = lot_info.lot
			index = lot_info.index
			is_obstacle = lot_info.is_obstacle
			if (is_obstacle):
				continue
			id_point = self.get_id(get_centroid(lot))
			is_banned = 0
			for tracked_index, tracked_object in enumerate(tracked_objects):
				if is_inside(tracked_object.last_detection.points, lot):
					is_banned = 1
					list_updated_id.add(id_point)
					is_parked[tracked_index] = 1
					lot_info.is_full = True
			self.parking_lots[index].is_banned = is_banned
		self.update_graph(list_updated_id)
		if (len(tracked_objects) == 0):
			return []
		not_parked_objects = np.array(tracked_objects)[np.array(is_parked) == 0]
		for not_parked_object in not_parked_objects:
			self.add_temp_vertex(not_parked_object.last_detection.points)
		return not_parked_objects

	def find_path(self, source_point, des_point):
		source_id = self.get_id(source_point)
		des_id = self.get_id(des_point)
		# print(source_id, des_id)
		path = nx.shortest_path(self.graph, source=source_id, target=des_id, weight='weight', method='dijkstra')
		distance = self.find_length(source_id, des_id)
		list_points = []
		for id in path:
			list_points.append(self.moveable_points[id])
		return list_points, distance / 20

	def find_length(self, source_id, des_id = None):
		path = nx.shortest_path_length(self.graph, source=source_id, target=des_id, weight='weight', method='dijkstra')
		return path

	def get_all_matchings(self, not_parked_objects):
		# print("object : ", not_parked_objects)
		min_weight_matching = nx.Graph()
		for not_parked_object in not_parked_objects:
			not_parked_point = tuple(not_parked_object.last_detection.points)
			not_parked_id = "object" + str(not_parked_object.id)
			shortest_lengths = self.find_length(self.get_id(not_parked_point))
			des_ids = []
			for lot_info in self.parking_lots:
				lot = lot_info.lot
				lot_id = "parking_lot" + str(lot_info.index)
				is_banned = lot_info.is_banned
				if (is_banned):
					continue
				lot_centroid = get_centroid(lot)
				des_id = self.get_id(lot_centroid)
				# print("info : ", not_parked_id, lot_id, des_id)
				min_weight_matching.add_edge(not_parked_id, lot_id, weight=shortest_lengths[des_id])
		all_matchings = nx.algorithms.bipartite.matching.minimum_weight_full_matching(min_weight_matching, top_nodes=None, weight='weight')
		pair_matchings = {}
		for object_string, not_parked_string in all_matchings.items():
			if (object_string.startswith("object") == 0):
				continue
			object_id = int(object_string.replace("object", ""))
			not_parked_id = int(not_parked_string.replace("parking_lot", ""))
			pair_matchings[object_id] = not_parked_id
		return pair_matchings

	def update_reserve(self, reserve_ids):
		for lot_info in self.parking_lots:
			if (lot_info.index in reserve_ids):
				lot_info.is_reserved = True
			else:
				lot_info.is_reserved = False

	def get_general_info(self):
		empty_slot = 0
		parked_slot = 0
		for lot_info in self.parking_lots:
			if (lot_info.is_obstacle):
				continue
			if (lot_info.is_full == 0):
				empty_slot = empty_slot + 1
			else:
				parked_slot = parked_slot + 1
		return {"Available slots" : empty_slot, "Parked slots" : parked_slot}

	def make_moveable_points(self):
		height, width, channels = self.image.shape
		dx = width // 20
		dy = height // 20
		moveable_points = []
		for ix in range(dx, width - dx, dx):
			for iy in range(dy, height - dy, dy):
				inside = False
				for lot_info in self.parking_lots:
					lot = lot_info.lot
					if (is_inside((ix, iy), lot)):
						inside = True
						break
				if (inside == False):
					moveable_points.append((ix, iy))
		return moveable_points

	def __init__(self, map_file, parking_lots_file, moveable_points_file):
		self.parking_lots = self.read_parking_lots(parking_lots_file)
		# self.moveable_points = self.read_moveable_points(moveable_points_file)
		self.image = cv2.imread(map_file)
		self.image[self.image > -1] = 255
		self.moveable_points = self.make_moveable_points()
		self.init_graph()
		print(len(self.parking_lots), len(self.moveable_points), len(self.graph))
		cv2.imshow("map", draw_map(self.image, self.parking_lots, self.moveable_points, self.graph))
		self.num_points = len(self.moveable_points)
		# path = nx.shortest_path(self.graph, source=1, target=33, weight='weight', method='dijkstra')
		# dijkstra_graph = self.gen_graph_path(path)
	


parking_map = ParkingMap('data/new_map/map.png', 'data/new_map/parking_lot.txt', "data/new_map/moveable_points.txt")
cv2.waitKey(0)


	
