# import the necessary packages
import numpy as np
import argparse
import cv2
import time
import os

from transform.transform import *
from object_tracker import *
from object_detector import *
from constant import *
from helper import *
from drawer import *
from parking_map import ParkingMap


# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", help = "path to the image file")
ap.add_argument("-v", "--video", help = "path to the video file")
ap.add_argument("-c", "--coords",
	help = "comma seperated list of source points")
ap.add_argument("-p", "--points", help="")
ap.add_argument("-2d", "--2dpoints", help="")
ap.add_argument("-out", "--output", help="")
args = vars(ap.parse_args())
# load the image and grab the source coordinates (i.e. the list of
# of (x, y) points)
# NOTE: using the 'eval' function is bad form, but for this example
# let's just roll with it -- in future posts I'll show you how to
# automatically determine the coordinates without pre-supplying them

max_width = 1286
max_height = 684

cap = cv2.VideoCapture(args["video"])
# cap = cv2.VideoCapture("rtsp://admin:RVDNYR@192.168.88.233/H.264")

pts = []

def write_point(pts, out_path):
    if (out_path != None):
        out_file = open(out_path, "w")
        for i in range(0, 4):
            write_string = str(pts[i][0]) + " " + str(pts[i][1])
            if (i != 3):
                write_string += " "
            out_file.write(write_string)

def add_point(event, x, y, flags, param):
    global pts
    if event == cv2.EVENT_LBUTTONDBLCLK:
        pts += [(x, y)]
        print(pts)
        if len(pts) == 4:
            write_point(pts, args["points"])
            # show_video()

def read_point_file(point_path):
    try:
        point_file = open(point_path, "r")
        line = point_file.readline()
        coor = line.split(" ")
        points = []
        if (len(coor) == 8):
            for i in range(0, 8, 2):
                    points += [(int(coor[i]), int(coor[i + 1]))]
        return points
    except:
        return []

def first_frame():
    global pts
    point_file = args["points"]
    print(point_file)
    if (point_file != None):
        pts = read_point_file(args["points"])
        if (len(pts) == 4):
            # ret, frame = cap.read()
            # frame = cv2.resize(frame, (max_width, max_height))
            # for point in pts:
            #     frame = cv2.circle(frame, (point[0], point[1]), 5, BLUE_COLOR, 2)
            # transformer = Transformer()
            # cv2.imshow("origin", frame)
            # transformer.four_point_transform(pts, 900, 600)
            # frame = transformer.get_warp(frame)
            # cv2.imshow("frame", frame)
            # cv2.waitKey(0)
            show_video()
            pass
        else:
            ret, frame = cap.read()
            frame = cv2.resize(frame, (max_width, max_height))
            cv2.imshow("frame", frame)
            cv2.setMouseCallback("frame", add_point)
            cv2.waitKey(0)
    else:
        ret, frame = cap.read()
        frame = cv2.resize(frame, (max_width, max_height))
        cv2.imshow("frame", frame)
        cv2.setMouseCallback("frame", add_point)
        cv2.waitKey(0)


fourcc = cv2.VideoWriter_fourcc(*'mp4v')
out_2d = cv2.VideoWriter('output/output_2d.mp4', fourcc, 20.0, (max_width,max_height))
out_origin = cv2.VideoWriter('output/out_origin.mp4', fourcc, 20.0, (max_width,max_height))

if (args["output"] is not None):
    print("path : ", args["output"])
    os.makedirs(args["output"], exist_ok=True)
    out_2d = cv2.VideoWriter(args["output"] + '/output_2d.mp4', fourcc, 20.0, (max_width,max_height))
    out_origin = cv2.VideoWriter(args["output"] + '/out_origin.mp4', fourcc, 20.0, (max_width,max_height))

def show_video():
    
    object_tracker = ObjectTracker()
    object_detector = ObjectDetector()
    transformer = Transformer()
    transformer.four_point_transform(pts, max_width, max_height)
    cnt = 0

    points_2d = read_point_file(args["2dpoints"])
    points_2d = np.array(points_2d)
    parking_map = ParkingMap('data/new_map/map.png', 'data/new_map/parking_lot.txt', "data/new_map/moveable_points.txt")
    while(cap.isOpened()):
        time1 = time.time()
        ret, frame = cap.read()
        if (ret is False):
            if (args["output"] is None):
              cap.set(cv2.CAP_PROP_POS_FRAMES, 0)
              ret, frame = cap.read()
            else:
              break;
        frame = cv2.resize(frame, (max_width, max_height))
        # for point in pts:
        #     frame = draw_circle(frame, point[0], point[1], BLUE_COLOR)
        frame_origin = frame
        print("get video time : ", time.time() - time1);
        detections = object_detector.detect_objects(frame)
        time1 = time.time()
        for detection in detections:
            original_point = detection.points
            frame_origin = draw_circle(frame_origin, original_point[0], original_point[1], GREEN_COLOR)
            detection.points = np.array(transformer.convert_point(np.array(detection.points)))

        base_point = transformer.convert_point(pts[0])
        ratio = points_2d.max(axis=0) - points_2d.min(axis=0)
        frame_2d = parking_map.convert_to_image()
        # for point in points_2d:
        #     frame_2d = draw_circle(frame_2d, point[0], point[1], BLUE_COLOR)
        for detection in detections:
            transform_point = detection.points
            map_point = points_2d[0] + (transform_point - base_point) / transformer.get_base_shape() * ratio
            map_point = map_point + [0, 30]
            # frame_2d = cv2.circle(np.float32(frame_2d), (int(map_point[0]), int(map_point[1])), 5, GREEN_COLOR, 2)
            detection.points = map_point

        tracked_objects = object_tracker.update(detections)
        # norfair.draw_tracked_objects(frame, tracked_objects)
        # frame = transformer.get_warp(frame)
        not_parked_objects = parking_map.update_map(tracked_objects)

        # norfair.draw_tracked_objects(frame_2d, tracked_objects)

        if (len(not_parked_objects)):
            pair_matchings = parking_map.get_all_matchings(not_parked_objects)
            reserve_ids = []
            for not_parked_object in not_parked_objects:
                not_parked_point = not_parked_object.last_detection.points
                parking_lot_id = pair_matchings[not_parked_object.id]
                path, distance = parking_map.find_path(tuple(not_parked_point), parking_map.get_parking_lot_centroid(parking_lot_id))
                frame_2d, is_moving = draw_moving_car(frame_2d, not_parked_object.id, not_parked_point[0], not_parked_point[1], distance)
                if (is_moving == True):
                    for i in range(1, len(path)):
                        frame_2d = draw_line_arrow(frame_2d, path[i - 1], path[i])
                    reserve_ids.append(parking_lot_id)
            parking_map.update_reserve(reserve_ids)
        frame_2d = draw_general_info(frame_2d, parking_map.get_general_info())
        time2 = time.time()
        frame_2d = frame_2d.astype(np.uint8)
        frame_origin = frame_origin.astype(np.uint8)
        frame = frame.astype(np.uint8)
        if (args["output"] is None):
            cv2.imshow("original vid", frame_origin)
            # cv2.imshow("transform vid", frame)
            cv2.imshow("2d vid", frame_2d)
            # ret, buffer = cv2.imencode('.jpg', frame_2d)
            # frame_2d = buffer.tobytes()
            # yield (b'--frame\r\n'
            #     b'Content-Type: image/jpeg\r\n\r\n' + frame_2d + b'\r\n')  # concat frame one by one and show result
        else:
            out_origin.write(frame_origin)
            out_2d.write(frame_2d)
        print("processing time : ", time2 - time1)

        if (cv2.waitKey(1) & 0xFF == ord('q')):
            break
    cap.release()
    cv2.destroyAllWindows()

first_frame()

# webapp

# from flask import Flask, render_template, Response

# app = Flask(__name__)


# @app.route('/video_feed')
# def video_feed():
    #Video streaming route. Put this in the src attribute of an img tag
    # return Response(show_video(), mimetype='multipart/x-mixed-replace; boundary=frame')


# @app.route('/')
# def index():
#     return render_template('index.html')


# if __name__ == '__main__':
#     first_frame()
#     port = int(os.environ.get('PORT', 5000))
#     app.run(host = '0.0.0.0', port = port)