#!/bin/bash

if [ "$1" ]; then
	echo "output_dir : " $1
	python3 main.py --video data/video_b1_start_15s.mp4 --points data/video_b1_start_15s.txt --2dpoints data/2dmap.txt --output $1
	exit 1
fi
python3 main.py --video data/video_b1_start_15s.mp4 --points data/video_b1_start_15s.txt --2dpoints data/2dmap.txt
