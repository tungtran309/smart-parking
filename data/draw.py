# import the necessary packages
import numpy as np
import argparse
import cv2

pts = []


# max_width = 1449
# max_height = 2048
max_width = 1286
max_height = 684

def write_point(pts, out_path):
    if (out_path != None):
        out_file = open(out_path, "w")
        for i in range(0, 4):
            write_string = str(pts[i][0]) + " " + str(pts[i][1])
            if (i != 3):
                write_string += " "
            out_file.write(write_string)

def add_point(event, x, y, flags, param):
    global pts
    if event == cv2.EVENT_LBUTTONDBLCLK:
        pts += [(x, y)]
        print(pts)
        if len(pts) == 4:
            show(np.asarray(pts))
            write_point(pts, "2dmap.txt")


# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", help = "path to the image file")
ap.add_argument("-c", "--coords",
	help = "comma seperated list of source points")
args = vars(ap.parse_args())
# load the image and grab the source coordinates (i.e. the list of
# of (x, y) points)
# NOTE: using the 'eval' function is bad form, but for this example
# let's just roll with it -- in future posts I'll show you how to
# automatically determine the coordinates without pre-supplying them
image = cv2.imread(args["image"])
# image = cv2.resize(image, (max_width, max_height))
cv2.imshow("image", image)

cv2.setMouseCallback("image", add_point)

# pts = np.array(eval(args["coords"]), dtype = "float32")
# apply the four point tranform to obtain a "birds eye view" of
# the image
cv2.waitKey(0)
