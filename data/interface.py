# import the necessary packages
import numpy as np
import argparse
import cv2
import time

GREEN_COLOR = (0, 255, 0)
BLUE_COLOR = (255, 0, 0)
RED_COLOR = (0, 0, 255)
THICKNESS = 2



parking_lot = []
cur_polygon = []


def get_centroid(lot):
    x = 0
    y = 0
    for point in lot:
        x = x + point[0]
        y = y + point[1]
    return (x // len(lot) - 5, y // len(lot) + 5)

def read_parking_lot(file):
    global parking_lot
    with open(args["out"], "r") as out_file:
        while (1):
            line = out_file.readline()
            if (len(line) == 0):
                break
            lot = []
            numbers = line.split(" ")
            size = len(numbers)
            for i in range(1, size - 1, 2):
                lot.append((int(numbers[i]), int(numbers[i + 1])))
            parking_lot.append((lot, int(numbers[0]), int(numbers[size - 1])))


def draw_lot(frame, lot, color):
    if (len(lot) == 0):
        return
    for i in range(1, len(lot)):
        frame = cv2.line(frame, lot[i - 1], lot[i], color, THICKNESS)
    frame = cv2.line(frame, lot[len(lot) - 1], lot[0], color, THICKNESS)

def draw_parking_lot():
    frame = image.copy()
    for lot_info in parking_lot:
        lot = lot_info[0]
        index = lot_info[1]
        is_banned = lot_info[2]
        color = (GREEN_COLOR if is_banned == 0 else RED_COLOR)
        draw_lot(frame, lot, color)
        cv2.putText(frame, str(index), get_centroid(lot), cv2.FONT_HERSHEY_SIMPLEX, 0.5, 255)
    draw_lot(frame, cur_polygon, BLUE_COLOR)

    # frame = cv2.circle(np.float32(frame), (pts[0], pts[1]), 5, BLUE_COLOR, 2
    
    print("parking_lot : ", parking_lot)
    cv2.imshow("image", frame)

pts = []

def add_parking_lot(event, x, y, flags, param):
    global cur_polygon, parking_lot
    if event == cv2.EVENT_LBUTTONDBLCLK:
        cur_polygon.append((x, y))
        draw_parking_lot()
        # print(cur_polygon)

# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", help = "path to the image file")
ap.add_argument("-o", "--out",
	help = "comma seperated list of source points")
args = vars(ap.parse_args())
image = cv2.imread(args["image"])
cv2.imshow("image", image)

cv2.setMouseCallback("image", add_parking_lot)

if ("out" in args):
    read_parking_lot(args["out"])
draw_parking_lot()

id = 0

while True:
    # cv2.imshow('image', image)
    x = cv2.waitKey(1)
    if (x == ord('q')):
        parking_lot.append((cur_polygon, len(parking_lot), 0))
        cur_polygon = []
        draw_parking_lot()
    if (x == ord('w')):
        parking_lot.append((cur_polygon, len(parking_lot), 1))
        cur_polygon = []
        draw_parking_lot()
    if (x == ord('d')):
        if (len(cur_polygon) > 0):
            cur_polygon = []
        else:
            parking_lot.pop()
        print("here")
        draw_parking_lot()
    if (x == ord('s')):
        with open(args["out"], "w") as out_file:
            for lot_info in parking_lot:
                lot = lot_info[0]
                index = lot_info[1]
                is_banned = lot_info[2]
                line = str(index) + " "
                for point in lot:
                    line = line + str(point[0]) + " " + str(point[1]) + " "
                line = line + str(is_banned);
                out_file.write(line + "\n")
        break;
    if (x == ord('x')):
        break;