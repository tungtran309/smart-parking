# import the necessary packages
import numpy as np
import argparse
import cv2
import time

GREEN_COLOR = (0, 255, 0)
BLUE_COLOR = (255, 0, 0)
RED_COLOR = (0, 0, 255)
THICKNESS = 2

max_width = 1286
max_height = 684

parking_lots = []
cur_polygon = []

def write_file(file_name):
    with open(file_name, "w") as out_file:
        for lot_info in parking_lots:
            lot = lot_info[0]
            index = lot_info[1]
            is_banned = lot_info[2]
            line = str(index) + " "
            for point in lot:
                line = line + str(point[0]) + " " + str(point[1]) + " "
            line = line + str(is_banned);
            out_file.write(line + "\n")

def get_centroid(lot):
    x = 0
    y = 0
    for point in lot:
        x = x + point[0]
        y = y + point[1]
    return (x // len(lot) - 5, y // len(lot) + 5)

def read_moveable_points(file):
    global cur_polygon
    with open(file, "r") as out_file:
        while (1):
            line = out_file.readline()
            if (len(line) == 0):
                break
            numbers = line.split(" ")
            cur_polygon.append((int(numbers[0]), int(numbers[1])))

def read_parking_lots(file):
    global parking_lots
    with open(file, "r") as out_file:
        id = 0
        while (1):
            line = out_file.readline()
            if (len(line) == 0):
                break
            lot = []
            numbers = line.split(" ")
            size = len(numbers)
            for i in range(1, size - 1, 2):
                lot.append((int(numbers[i]), int(numbers[i + 1])))
            index = id
            is_banned = int(numbers[size - 1])
            parking_lot = (lot, index, is_banned)
            if (parking_lot not in parking_lots):
                parking_lots.append(parking_lot)
            id = id + 1


def draw_lot(frame, lot, color):
    if (len(lot) == 0):
        return
    for i in range(1, len(lot)):
        frame = cv2.line(frame, lot[i - 1], lot[i], color, THICKNESS)
    frame = cv2.line(frame, lot[len(lot) - 1], lot[0], color, THICKNESS)

def draw_parking_lots():
    frame = image.copy()
    for lot_info in parking_lots:
        lot = lot_info[0]
        index = lot_info[1]
        is_banned = lot_info[2]
        color = (GREEN_COLOR if is_banned == 0 else RED_COLOR)
        draw_lot(frame, lot, color)
        cv2.putText(frame, str(index), get_centroid(lot), cv2.FONT_HERSHEY_SIMPLEX, 0.5, 255)
    draw_lot(frame, cur_polygon, BLUE_COLOR)

    # frame = cv2.circle(np.float32(frame), (pts[0], pts[1]), 5, BLUE_COLOR, 2
    
    print("parking_lots : ", parking_lots)
    cv2.imshow("image", frame)

is_draw_lot = True

def draw_moveable_points():
    frame = image.copy()
    for lot_info in parking_lots:
        lot = lot_info[0]
        index = lot_info[1]
        is_banned = lot_info[2]
        color = (GREEN_COLOR if is_banned == 0 else RED_COLOR)
        draw_lot(frame, lot, color)
        cv2.putText(frame, str(index), get_centroid(lot), cv2.FONT_HERSHEY_SIMPLEX, 0.5, 255)
    for pts in cur_polygon:
        frame = cv2.circle(frame, (pts[0], pts[1]), 5, BLUE_COLOR, 2)
    cv2.imshow("image", frame)

def add_parking_lots(event, x, y, flags, param):
    global cur_polygon, parking_lots
    if event == cv2.EVENT_LBUTTONDBLCLK:
        cur_polygon.append((x, y))
        if (is_draw_lot):
            draw_parking_lots()
        else:
            draw_moveable_points()
        # print(cur_polygon)


def read_image(image_file):
    global parking_lots
    frame = cv2.imread(image_file)
    frame = cv2.resize(frame, (max_width, max_height))
    cv2.imwrite(image_file, frame)
    frame_gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    width, height = frame_gray.shape
    print(width, height)
    MAX = 1e9
    cnt = 0
    frame_gray = ((frame_gray >= 255) == 0)
    frame_gray.dtype = 'uint8'
    # print(frame_gray)
    # cv2.imshow("gray", frame_gray * 255)
    # cv2.waitKey(1)
    # return frame
    for i in range(width):
        for j in range(height):
            if (frame_gray[i][j]):
                continue
            queue = [(i, j)]
            top_point = (MAX, MAX)
            right_point = (-MAX, MAX)
            bot_point = (-MAX, -MAX)
            left_point = (MAX, -MAX)
            area = 0
            while(queue):
                x, y = queue.pop()
                if (top_point[1] > y or (top_point[1] == y and top_point[0] > x)):
                    top_point = (x, y)
                if (right_point[0] < x or (right_point[0] == x and right_point[1] > y)):
                    right_point = (x, y)
                if (bot_point[1] < y or (bot_point[1] == y and bot_point[0] < x)):
                    bot_point = (x, y)
                if (left_point[0] > x or (left_point[0] == x and left_point[1] < y)):
                    left_point = (x, y)
                for dx in range(-1, 2):
                    for dy in range(-1, 2):
                        xx = x + dx
                        yy = y + dy
                        if (xx < 0 or xx >= width or yy < 0 or yy >= height):
                            continue
                        if (frame_gray[xx][yy]):
                            continue
                        # print(x, y, xx, yy)
                        queue.append((xx, yy))
                        frame_gray[xx][yy] = 1
                area = area + 1
            if (area > 2000):
                lot = [top_point, right_point, bot_point, left_point]
                for i in range(0, len(lot)):
                    lot[i] = (lot[i][1], lot[i][0])
                print(area, lot)
                parking_lots.append((lot, cnt, 0))
                cnt = cnt + 1
    return frame

# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", help = "path to the image file")
ap.add_argument("-o", "--map",
    help = "map storerage file")
ap.add_argument("-p", "--points",
    help = "point storerage file")
args = vars(ap.parse_args())

image = read_image(args["image"])
image = image * 0 + 255
# image = cv2.imread(args["image"])

cv2.imshow("image", image)

cv2.setMouseCallback("image", add_parking_lots)

if ("map" in args):
    read_parking_lots(args["map"])
draw_parking_lots()

while True:
    # cv2.imshow('image', image)
    x = cv2.waitKey(1)
    if (x == ord('q')):
        parking_lots.append((cur_polygon, len(parking_lots), 0))
        cur_polygon = []
        draw_parking_lots()
    if (x == ord('w')):
        parking_lots.append((cur_polygon, len(parking_lots), 1))
        cur_polygon = []
        draw_parking_lots()
    if (x == ord('d')):
        if (len(cur_polygon) > 0):
            cur_polygon = []
        else:
            parking_lots.pop()
        draw_parking_lots()
    if (x == ord('s')):
        write_file(args["map"])
        break;
    if (x == ord('x')):
        write_file('unsave.txt')
        break;
cur_polygon = []
if ("points" in args):
    read_moveable_points(args["points"])
is_draw_lot = False
draw_moveable_points()

while True:
    x = cv2.waitKey(1)
    if (x == ord('d')):
        cur_polygon.pop()
        draw_moveable_points()
    if (x == ord('s')):
        with open(args["points"], "w") as out_file:
            for pts in cur_polygon:
                out_file.write(str(pts[0]) + " " + str(pts[1]) + "\n")
        break;
    if (x == ord('x')):
        out_file = open('unsave_point.txt', "w")
        for pts in cur_polygon:
            out_file.write(str(pts[0]) + " " + str(pts[1]) + "\n")
        break;

