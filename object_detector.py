import numpy as np
import torch
import time
import cv2
import norfair
import torch
from models import Yolov4
from tool.torch_utils import do_detect
from norfair import Detection, Tracker, Video



class YOLO:
	def __init__(self, weightfile):
		self.use_cuda = torch.cuda.is_available()
		self.model = Yolov4(yolov4conv137weight=None, n_classes=80, inference=True)
		pretrained_dict = torch.load(
			weightfile, map_location=torch.device("cuda" if self.use_cuda else "cpu")
		)
		self.model.load_state_dict(pretrained_dict)

		if self.use_cuda:
			self.model.cuda()

	def __call__(self, img):
		width, height = 416, 416
		sized = cv2.resize(img, (width, height))
		sized = cv2.cvtColor(sized, cv2.COLOR_BGR2RGB)

		boxes = do_detect(self.model, sized, 0.4, 0.6, self.use_cuda)
		return boxes[0]


def get_centroid(yolo_box, img_height, img_width):
	x1 = yolo_box[0] * img_width
	y1 = yolo_box[1] * img_height
	x2 = yolo_box[2] * img_width
	y2 = yolo_box[3] * img_height
	return np.array([(x1 + x2) / 2, (y1 + y2) / 2])

class ObjectDetector:
	def __init__(self):
		self.model = YOLO("config/yolov4.pth")  # set use_cuda=False if using CPU
	def detect_objects(self, frame):
		time1 = time.time()
		detections = self.model(frame)
		detections = [
			Detection(get_centroid(box, frame.shape[0], frame.shape[1]), data=box)
			for box in detections
			if box[-1] == 2
		]
		time2 = time.time()
		print("detection time : ", time2 - time1)
		return detections

