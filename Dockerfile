FROM python:3.6

RUN apt-get update -y && \
    apt-get install -y python3-pip python3-dev
COPY ./requirements.txt /app/requirements.txt

ENV APP_HOME /app
WORKDIR $APP_HOME

COPY . /app

RUN pip install torch==1.8.1+cpu torchvision==0.9.1+cpu torchaudio==0.8.1 -f https://download.pytorch.org/whl/torch_stable.html
RUN pip3 install -r requirements.txt

# ENTRYPOINT ["python3 main.py"]
CMD ["python3","main.py","--video", "data/video_b1_start_15s.mp4", "--points", "data/video_b1_start_15s.txt", "--2dpoints", "data/2dmap.txt"]
