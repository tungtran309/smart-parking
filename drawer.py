import numpy as np
from constant import *
from configs import *
from helper import *
import cv2


parked_car = cv2.imread(PARKED_CAR_IMAGE)
parked_car = cv2.resize(parked_car, (ICON_SIZE))
moving_car = cv2.imread(MOVING_CAR_IMAGE)
moving_car = cv2.resize(moving_car, (ICON_SIZE))
cnt = 0

def draw_circle(frame, x_offset, y_offset, color, size = 5):
    x_offset = int(x_offset)
    y_offset = int(y_offset)
    frame = cv2.circle(np.float32(frame), (x_offset, y_offset), size, color, 2)
    return frame

def draw_lot(frame, lot, color):
    if (len(lot) == 0):
        return
    for i in range(1, len(lot)):
        frame = cv2.line(frame, lot[i - 1], lot[i], color, THICKNESS)
    frame = cv2.line(frame, lot[len(lot) - 1], lot[0], color, THICKNESS)

def draw_parked_car(frame, x_offset, y_offset):
    x_offset = x_offset - ICON_SIZE[0] // 2
    y_offset = y_offset - ICON_SIZE[1] // 2 + 10
    frame[y_offset:y_offset + parked_car.shape[0], x_offset:x_offset + parked_car.shape[1]] = parked_car
    return frame

car_positions = dict()

def is_moving_car(id, x_offset, y_offset):
    if (id not in car_positions):
        car_positions[id] = []
    car_positions[id].append((x_offset, y_offset))
    if (len(car_positions[id]) > 150):
        del car_positions[id][0]
    total_dis = 0
    for i in range(10, len(car_positions[id]), 10):
        dis = euclidean_distance(car_positions[id][i - 10], car_positions[id][i])
        if (dis >= 5):
            total_dis += dis
    if (len(car_positions[id]) < 150):
        return True
    return total_dis >= 50

def draw_moving_car(frame, id, x_offset, y_offset, distance):
    is_moving = is_moving_car(id, x_offset, y_offset)
    x_offset = int(x_offset)
    y_offset = int(y_offset)
    color = BLUE_COLOR if is_moving else RED_COLOR
    frame = draw_circle(frame, x_offset, y_offset, color, 7)
    cv2.putText(frame, "vehicle : " + str(id), (x_offset - 50, y_offset - 20), cv2.FONT_HERSHEY_SIMPLEX, 0.4, color, 1)
    if (is_moving):
        cv2.putText(frame, "distance : " + format(distance,".2f") + "(m)", (x_offset - 50, y_offset - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.4, color, 1)
    else:
        cv2.putText(frame, "standstill", (x_offset - 50, y_offset - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.4, color, 1)
    # frame[y_offset:y_offset + moving_car.shape[0], x_offset:x_offset + moving_car.shape[1]] = moving_car
    return frame, is_moving

def draw_parking_lots(frame, parking_lots):
    global cnt
    cnt = cnt + 1
    for lot_info in parking_lots:
        lot = lot_info.lot
        index = lot_info.index
        is_banned = lot_info.is_banned
        is_obstacle = lot_info.is_obstacle
        is_full = lot_info.is_full
        is_reserved = lot_info.is_reserved
        name = lot_info.name
        if (is_obstacle):
            continue
        color = (GREEN_COLOR if is_banned == 0 else (RED_COLOR if is_obstacle  else YELLOW_COLOR))
        if (is_reserved and (cnt % 10 == 0)):
            color = ORANGE_COLOR
        draw_lot(frame, lot, color)
        center = get_centroid(lot)
        if (is_full):
            frame = draw_parked_car(frame, center[0], center[1])
        cv2.putText(frame, name, (center[0] - 10, center[1] - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.4, BLUE_COLOR, 1)
    return frame    

def draw_moveable_points(frame, moveable_points):
    # frame = image.copy()
    for pts in moveable_points:
        frame = cv2.circle(frame, (int(pts[0]), int(pts[1])), 5, BLUE_COLOR, 2)
    return frame

def draw_graph(frame, moveable_points, graph):
    for edge in graph.edges:
        # print(edge)
        point_u = moveable_points[edge[0]]
        point_v = moveable_points[edge[1]]
        frame = cv2.line(frame, (int(point_u[0]), int(point_u[1])), (int(point_v[0]), int(point_v[1])), BLUE_COLOR, THICKNESS)        
    return frame

def draw_line_arrow(frame, src_point, des_point):
    # vector = (des_point[0] - src_point[0], des_point[1] - src_point[1])
    # vector = vector / np.linalg.norm(vector) * 30
    # vector = vector / 2
    # des_point = src_point + vector
    return cv2.arrowedLine(frame, (int(src_point[0]), int(src_point[1])), (int(des_point[0]), int(des_point[1])), AQUA_COLOR, 3, tipLength=0.2) 

def draw_general_info(frame, general_info):
    x_offset = 10
    y_offset = 30
    for title in general_info:
        text = title + " : " + str(general_info[title])
        cv2.putText(frame, text, (x_offset, y_offset), cv2.FONT_HERSHEY_SIMPLEX, 0.5, BLACK_COLOR, 1)
        y_offset = y_offset + 20
    return frame

def draw_map(image, parking_lots, moveable_points = None, graph = None):
    frame = image.copy()
    # cv2.imshow("image", frame)
    frame = draw_parking_lots(frame, parking_lots)
    if (moveable_points is not None):
        frame = draw_moveable_points(frame, moveable_points)
    if (graph is not None):
        frame = draw_graph(frame, moveable_points, graph)
    # cv2.imshow("image", frame)
    return frame

