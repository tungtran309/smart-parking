from norfair import Detection, Tracker, Video
from tool.utils import load_class_names, plot_boxes_cv2
import numpy as np


MAX_DISTANCE_BETWEEN_POINTS = 30

def euclidean_distance(detection, tracked_object):
		return np.linalg.norm(detection.points - tracked_object.estimate)

class ObjectTracker:

	def __init__(self):
		self.tracker = Tracker(
			distance_function=euclidean_distance,
			distance_threshold=MAX_DISTANCE_BETWEEN_POINTS,
		)
	def update(self, detections):
		return self.tracker.update(detections=detections)

