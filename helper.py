import numpy as np

def get_centroid(lot):
    x = 0
    y = 0
    for point in lot:
        x = x + point[0]
        y = y + point[1]
    return (x // len(lot), y // len(lot))

def euclidean_distance(p, q):
	return np.linalg.norm((p[0] - q[0], p[1] - q[1]))


def ccw(A,B,C):
    return (C[1]-A[1]) * (B[0]-A[0]) > (B[1]-A[1]) * (C[0]-A[0])

# Return true if line segments AB and CD intersect
def intersect(A,B,C,D):
    return ccw(A,C,D) != ccw(B,C,D) and ccw(A,B,C) != ccw(A,B,D)

def is_lot_intersection(p, q, lot):
	for i in range(1, len(lot)):
		if (intersect(p, q, lot[i - 1], lot[i])):
			return True
		if (intersect(q, p, lot[0], lot[len(lot) - 1])):
			return True
	return False

def is_inside(point, lot):
	return is_lot_intersection(get_centroid(lot), point, lot) == 0



# G = {
# 	'Ann': {'RB': 3, 'CAM': 2, 'GK': 1},
# 	'Ben': {'LW': 3, 'S': 2, 'CM': 1},
# 	'Cal': {'CAM': 3, 'RW': 2, 'SWP': 1},
# 	'Dan': {'S': 3, 'LW': 2, 'GK': 1},
# 	'Ela': {'GK': 3, 'LW': 2, 'F': 1},
# 	'Fae': {'CM': 3, 'GK': 2, 'CAM': 1},
# 	'Gio': {'GK': 3, 'CM': 2, 'S': 1},
# 	'Hol': {'CAM': 3, 'F': 2, 'SWP': 1},
# 	'Ian': {'S': 3, 'RW': 2, 'RB': 1},
# 	'Jon': {'F': 3, 'LW': 2, 'CB': 1},
# 	'Kay': {'GK': 3, 'RW': 2, 'LW': 1, 'LB': 0}
# }

# print(algorithm.find_matching(G, matching_type = 'max', return_type='list'))
